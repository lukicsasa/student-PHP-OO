<?php
session_start();


class Student
{
    private $ime;
    private $prezime;
    private $indeks;
    private $mesto;
    private $ocene = array();

    public function __construct($ime, $prezime, $indeks, $mesto)
    {
        $this->ime = $ime;
        $this->prezime = $prezime;
        $this->indeks = $indeks;
        $this->mesto = $mesto;
    }

    public function getIme()
    {
        return $this->ime;
    }

    public function getPrezime()
    {
        return $this->prezime;
    }

    public function getIndeks()
    {
        return $this->indeks;
    }

    public function getMesto()
    {
        return $this->mesto;
    }

    public function getOcene()
    {
        return $this->ocene;
    }

    public function dodajStudenta()
    {
        if (!isset($_SESSION['studenti'])) {
            $_SESSION['studenti'] = array();
        }
        array_push($_SESSION['studenti'], $this->indeks . "");
        $_SESSION['studenti'][$this->indeks . ""][] = $this;

    }

    public function dodajPredmet($predmet)
    {
        array_push($this->ocene, $predmet);
    }

}

class Predmet
{
    private $naziv;
    private $ocena;

    public function __construct($naziv, $ocena)
    {
        $this->naziv = $naziv;
        $this->ocena = $ocena;
    }

    public function getNaziv()
    {
        return $this->naziv;
    }

    public function getOcena()
    {
        return $this->ocena;
    }
}


function ucitajIme($indeks)
{
    return $_SESSION['studenti'][$indeks . ""][0]->getIme();
}

function ucitajPrezime($indeks)
{
    return $_SESSION['studenti'][$indeks . ""][0]->getPrezime();
}

function ucitajIndeks($indeks)
{
    return $_SESSION['studenti'][$indeks . ""][0]->getIndeks();
}

function ucitajMesto($indeks)
{
    return $_SESSION['studenti'][$indeks . ""][0]->getMesto();
}

function ispisiPredmete($indeks)
{
    for ($i = 0; $i < count($_SESSION['studenti'][$indeks . ""][0]->getOcene()); $i++) {
        $tmp = $_SESSION['studenti'][$indeks . ""][0]->getOcene()[$i];
        echo "<tr><td>" . ($i + 1) . "</td><td>" . $tmp->getNaziv() . "</td><td>" . $tmp->getOcena() . "</td></tr>";
    }
}

function pronadjiStudenta($indeks)
{
    return $student = $_SESSION['studenti'][$indeks . ""][0];
}


$proveraImePrezime = "/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u";
$proveraIndeks = "/^[0-9]{8}$/";
$proveraOcena = "/^([5-9]|10){1}$/";

$flag1 = true;
$flag2 = true;

if (isset($_POST['unesiStudenta'])) {

    if (isset($_POST['ime']) && preg_match($proveraImePrezime, $_POST['ime'])) {
        $ime = $_POST['ime'];
    } else {
        echo "<p style='color: red;font-size: 20px'> Morate uneti ime studenta!</p>";
        $flag1 = false;

    }
    if (isset($_POST['prezime']) && preg_match($proveraImePrezime, $_POST['prezime'])) {
        $prezime = $_POST['prezime'];

    } else {
        echo "<p style='color: red;font-size: 20px'> Morate uneti prezime studenta!</p>";
        $flag1 = false;

    }
    if (isset($_POST['indeks']) && preg_match($proveraIndeks, $_POST['indeks'])) {
        $indeks = $_POST['indeks'];
        $_SESSION['student'] = $indeks;

    } else {
        echo "<p style='color: red;font-size: 20px'> Morate uneti broj indeksa studenta!</p>";
        $flag1 = false;

    }

    if (isset($_POST['cmb'])) {
        $mesto = $_POST['cmb'];
    } else {
        $flag1 = false;
    }


    if ($flag1) {
        $student = new Student($ime, $prezime, $indeks, $mesto);
        $student->dodajStudenta();
    }
}

if (isset($_POST['unesiPredmet'])) {
    if (isset($_POST['predmet'])) {
        $nazivPredemta = $_POST['predmet'];
    } else {
        echo "<p style='color: red;font-size: 20px'> Morate uneti naziv predmeta</p>";
        $flag2 = false;
    }
    if (isset($_POST['ocena']) && preg_match($proveraOcena, $_POST['ocena'])) {
        $ocena = $_POST['ocena'];
    } else {
        echo "<p style='color: red;font-size: 20px'> Morate uneti ocenu od 5 do 10</p>";
        $flag2 = false;

    }

    if ($flag2) {
        $predmet = new Predmet($nazivPredemta, $ocena);
        $student1 = pronadjiStudenta($_SESSION['student']);
        $student1->dodajPredmet($predmet);
    }


}

?>

<!DOCTYPE html>
<html>
<header>
    <title>Student</title>
</header>
<body>
<p style="margin: 10px auto -45px 590px;color:green;font-size: 25px">Unesite studenta:</p>
<div style="margin: 50px auto 0 auto;border: 2px solid green; width: 350px">
    <form action="index.php" method="post" style="margin: 10px;">
        Ime: <input type="text" name="ime" style="margin-left: 5px;margin-bottom: 5px"
                    value="<?php if (isset($_SESSION['student'])) echo ucitajIme($_SESSION['student']) ?>"> <br>
        Prezime: <input type="text" name="prezime" style="margin-left: 5px;margin-bottom: 5px"
                        value="<?php if (isset($_SESSION['student'])) echo ucitajPrezime($_SESSION['student']) ?>"> <br>
        Broj indeksa: <input type="text" name="indeks" style="margin-left: 5px;margin-bottom: 5px"
                             value="<?php if (isset($_SESSION['student'])) echo ucitajIndeks($_SESSION['student']) ?>">
        <br>
        Mesto: <select name="cmb" style="margin-left: 5px;margin-bottom: 5px">
            <option selected="selected"
                    disabled="disabled"> <?php if (isset($_SESSION['student'])) echo ucitajMesto($_SESSION['student']) ?> </option>
            <option value="Beograd"> Beograd</option>
            <option value="Sabac"> Sabac</option>
            <option value="Novi Sad"> Novi Sad</option>
            <option value="Nis"> Nis</option>
        </select> <br>
        <input type="submit" name="unesiStudenta" value="Unesi studenta"
               style="margin: 5px auto auto 100px;width: 120px;">
    </form>
</div>



<div style="margin: 20px auto 20px  auto;border: 2px solid green; width: 350px">
    <table border="1" style="border: 1px solid green;border-spacing: 3px;width: 350px">
        <tr>
            <th>Rb.</th>
            <th> Predmet</th>
            <th>Ocena</th>
        </tr>
        <?php
        if (isset($_SESSION['student'])) ispisiPredmete($_SESSION['student']);
        ?>
    </table>
</div>
<p style="margin: -5px auto 5px 610px;color:green;font-size: 25px">Unesite ispit:</p>
<div style="margin:  auto auto 50px  auto;border: 2px solid green; width: 350px">
    <form action="index.php" method="post" style="margin: 10px;">
        Predmet: <input type="text" name="predmet" style="margin-left: 5px;margin-bottom: 5px"> <br>
        Ocena: <input type="text" name="ocena" style="margin-left: 5px;margin-bottom: 5px"> <br>
        <input type="submit" name="unesiPredmet" value="Unesi predmet" style="margin: 5px auto auto 100px;width: 120px">
    </form>
</div>
</body>
</html>